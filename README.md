# Page Scraper

Page Scraper takes in a file, such as an html file, looks for the defined values and outputs a csv file with headers and the data in of course csv format.  Almost like an offline web scraper. Used this for extracting assets from Tenable.io since they do not export a list and we wanted to see what assets were located in our various systems.