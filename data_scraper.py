##
##  Data Scraper
##  For grabbing certain field values out of a file
##  by James Caldwell
##  February 5, 2019
##


from argparse import ArgumentParser

import os.path
import datetime as dt
import traceback

arg_error = "Error executing program, use -h for help"

## Fields are what is to be extracted, but here they will end up being the column name for the output file
fields = ["Name,", "IP,", "OS,\n"]
result = []
num_fields = len(fields)

## Debugging field length
# print("Field length is " + str(num_fields))

headers = ''.join(fields)

## Let's measure how long our program takes to run, good for large datasets and to see how
## I can improve performance later
start = dt.datetime.now()

parser = ArgumentParser()
parser.add_argument("-f", "--file", dest="input_file", help="Open specified file")
args = parser.parse_args()
input_file = args.input_file

outfile = args.input_file + "_mined.csv"
row_num = 0
name_cnt = 0


def write_line(line):
    with open(outfile, "a") as out:
        out.write(line + "\n")


def is_asset(line):
    global row_num
    global name_cnt
    if "<td class=\"asset-name " in line:
        print("\n<---- Found an asset-name at line " + str(row_num) + " ---->")
        name_cnt += 1
        return True

    return False


def get_asset(line):
    line = line.strip()
    # print(line + " at line " + str(row_num))
    # print(line)
    line_builder(line)

    return line


def is_ip(line):
    if "<td class=\"ip-address" in line:
        # print("Found IP Address")
        return True

    return False


def get_ip(line):
    # line = line.replace(" ", "")
    line = line.strip()
    # print(line + " at line " + str(row_num))
    #  print(line)
    line_builder(line)
    return line


def is_os(line):
    if "<td class=\"operating-system" in line:
        #  print("Found OS")
        return True

    return False


def get_os(line):
    # line = line.replace(" ", "")
    line = line.strip()
    # print(line + " at line " + str(row_num))
    # print(line)
    line_builder(line)
    return line


##Build the new line to be written to file
def line_builder(word):
    global result


    result.append(word + ",")

    if len(result) == 3:

        print(str(result))
        result_line = ''.join(result)
        write_line(result_line)
           # print("\n\n" + str(result))
        result = []



def read_line(file):
    global row_num
    with file as fp:
        line = fp.readline()
        #  cnt = 1

        while line:
            # line =
            # find_values(line)
            row_num += 1
            if is_asset(line):

                line = fp.readline()
                get_asset(line)

            elif is_ip(line):

                line = fp.readline()
                get_ip(line)

            elif is_os(line):
                line = fp.readline()
                get_os(line)
            else:
                line = fp.readline()

    # write_line(line)

    #  cnt += 1
    #  print("Reading line........" + str(cnt))


def main():
    try:

        global input_file
        global name_cnt
        with open(input_file)as input_file:
            text = input_file
            #   print(str(result))
            if os.path.exists(outfile):
                os.remove(outfile)
                with open(outfile, "a") as out:
                    out.write(headers)
                    print("The file " + outfile + " replaced the previous file")
            else:
                try:
                    with open(outfile, "a") as out:
                        out.write(headers)
                        print("The file " + outfile + " has been created")
                except IOError:
                    print("File write error")
            read_line(text)
            end = dt.datetime.now()
            print("\n<---------- Extraction took {} s. ---------->".format((end - start).seconds))
            print("Found a total of " + str(name_cnt) + " assets\n")

    except TypeError:
        print(arg_error)
        traceback.print_exc()


main()
